<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AdController@index');
Route::get('/{id}', 'AdController@show')->where('id', '[0-9]+');
Route::get('/create', 'AdController@create');
Route::post('/store', 'AdController@store');
Route::get('/{id}/edit', 'AdController@edit')->where('id', '[0-9]+');
Route::post('/{id}/update', 'AdController@update')->where('id', '[0-9]+');
Route::get('/{id}/delete', 'AdController@destroy')->where('id', '[0-9]+');

Route::get('/login', 'SessionController@getLogin');
Route::post('/login', 'SessionController@postLogin');

Route::get('/logout','SessionController@logout');

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
