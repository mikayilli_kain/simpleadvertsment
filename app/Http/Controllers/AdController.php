<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Ad;

use Session;

use Auth;

use Gate;

class AdController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index','show','create'] ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $ads = Ad::paginate(5);

        return view('home',compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(! auth()->check())
        {
            Session::flash('message','for create an Ad you have to login');

           return  redirect('/login');
        }

        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $req->validate([
            'title' => 'required | min:2 | max : 200',
            'description' => 'required | min:2'
        ]);

        $ad = new Ad;

        $ad->title = ucfirst($req->title);
        $ad->description = ucfirst($req->description);
        $ad->user_id = Auth::id();

        $ad->save();

        return redirect('/'.$ad->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $ad = Ad::findOrFail($id);

        return view('show',compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad = Ad::findOrFail($id);

        if(Gate::denies('affect-ad',$ad))
        {   
           Session::flash('message','You Have Not Permission');

           return  redirect('/');
        }

        return view('edit',compact('ad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {   
        $ad = Ad::findOrFail($id);

        if(Gate::denies('affect-ad',$ad))
        {   
           Session::flash('message','You Have Not Permission');
           
           return  redirect('/');
        }

        $req->validate([
            'title' => 'required | min:2',
            'description' => 'required | min:2'
        ]);

        $ad->title = ucfirst($req->title);
        $ad->description = ucfirst($req->description);
        $ad->user_id = Auth::id();

        $ad->update();

        return redirect('/'.$ad->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad = Ad::findOrFail($id);

        if(Gate::denies('affect-ad',$ad))
        {   
           Session::flash('message','You Have Not Permission');
           
           return  redirect('/');
        }

        $ad->delete();

        return redirect('/');
    }
}
