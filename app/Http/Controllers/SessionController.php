<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SessionController extends Controller
{	

	public function getLogin(Request $req)
    {
 		if(auth()->check())
 			return redirect('/');

 		return view('login');
 	}

    public function postLogin(Request $req)
    {	
    	//return $req->all();

 		if(auth()->check())
 			return redirect('/');

 		$req->validate([
 				'username' => 'required | min:1 | max: 191',
 				'password' => 'required | min:4 | max: 191',
 		]);

 		$uName = $req->username;
 		$pass  = $req->password;

 		$user = \App\User::where('username',$uName)
 						 ->where('password',bcrypt($pass))
 						 ->get();
 		
 		if(isset($user[0]))
 		{
 			Auth::loginUsingId($user[0]->id);
 			return redirect('/');
 		}
 		else
 		{	
 			$req->validate([
 				'username' => 'required | unique:users| min:1 | max: 191',
 				'password' => 'required | min:4 | max: 191',
 			]);

 			$newUser = new \App\User;

 			$newUser->username = $uName;
 			$newUser->password = bcrypt($pass);

 			$newUser->save();

 			Auth::loginUsingId($newUser->id);
 			return redirect('/');
 		}
    }


    public function logout()
    {
    	Auth::logout();
    	return redirect('/');
    }
}
