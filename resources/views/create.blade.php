@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

			@if(count($errors))              
                    
                <div class="alert alert-danger text-center" role="alert">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>                  
               
            @endif

        	<form action="/store" method="POST">

        		@csrf 

	            <div class="card border-secondary">
	                <div class="card-header">Create an Ad</div>

	                <div class="card-body">
	                     <div class="form-group">
	                        <div class="col-sm-10 offset-sm-1 col-xs-12">
	                          <label for="name">Title</label>
	                          <input type="text" name="title" class="form-control" required autofocus>
	                        </div>
	                      </div>
						  
						  <div class="form-group">
	                        <div class="col-sm-10 offset-sm-1 col-xs-12">
	                          <label for="name">Description</label>
	                          <textarea name="description" cols="10" rows="5" class="form-control" style="resize: vertical"; required ></textarea>
	                        </div>
	                      </div>
	                </div>

	                <div class="card-footer">
	                	<button class="btn btn-sm btn-primary">Create</button>
	                </div>
	            </div>
            </form>
        </div>
    </div>
</div>
@endsection
