@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(Session::has('message'))
                <div class="alert alert-warning text-center" role="alert">
                  {{ Session::get('message') }}
                </div>
            @endif

            @forelse($ads as $ad)

                <div class="card">
                    
                    <div class="card-body">
                        
                        <h3><a href="/{{ $ad->id }}">{{  \Illuminate\Support\Str::limit($ad->title, 40,' ...') }}</a></h3>

                        <hr>
                       
                        <h5>{{  \Illuminate\Support\Str::limit($ad->description, 200,' ...') }}</h5>

                    </div>

                    <div class="card-footer text-center">
                        <span class="float-left">Author: {{ Auth::id() == $ad->user_id ? 'You' : ($ad->user->username ?? '???') }}</span>
                        
                        @can('affect-ad' , $ad)
                            <span class="text-center">
                                <a href="/{{ $ad->id }}/edit" class=" btn btn-info btn-sm">Edit</a>
                                <a href="/{{ $ad->id }}/delete" class=" btn btn-danger btn-sm" onclick="return del()">Delete</a>
                            </span>
                        @endcan

                        <span class="float-right">Created at: {{ $ad->created_at->diffForHumans() }}</span>

                    </div>
                </div>
                
                <br>
            @empty

               <div class="card text-center">
                    
                    <div class="card-body">
                       Ads Not Created Yet . Be The Frist
                    </div>

                </div>
            @endforelse

            <div class="col-xs-12 float-right">
                {{ $ads->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
