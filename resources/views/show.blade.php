@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                	<span>Author: {{ Auth::id() == $ad->user_id ? 'You' : $ad->user->name }} </span>
                	<span class="float-right">Created At: {{ $ad->created_at->diffForHumans() }} </span>
                </div>
                <div class="card-body">
                	<h3>Title</h3>
                	
                	<h5>{{ $ad->title }}</h5>

                	<hr>

                	<h3>Description</h3>
                	
                	<h5>{{ $ad->description }}</h5>

                </div>	
				
				@can('affect-ad',$ad)
	                <div class="card-footer">
	                	<a href="/{{ $ad->id }}/edit" class="btn btn-info btn-sm">Edit</a>
	                	<a href="/{{ $ad->id }}/delete" class="btn btn-danger btn-sm" onclick="return del()">Delete</a>
	                </div>	
                @endcan			
            </div>
        </div>
    </div>
</div>



@endsection
